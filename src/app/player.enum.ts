export enum CellState {
  Empty = '',
  X = 'X',
  O = 'O'
}
