import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { CellState } from './player.enum';
import { Subject } from 'rxjs/Subject';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import 'rxjs/add/operator/scan';
import 'rxjs/add/operator/startWith';

enum ActionType {
  Shoot = 'SHOOT',
  Restart = 'RESTART'
}

interface ShootAction {
  type: ActionType.Shoot;
  payload: { x: number, y: number };
}

interface RestartAction {
  type: ActionType.Restart;
}

type Action = ShootAction | RestartAction;

const initial = [
  [CellState.Empty, CellState.Empty, CellState.Empty],
  [CellState.Empty, CellState.Empty, CellState.Empty],
  [CellState.Empty, CellState.Empty, CellState.Empty],
];

function ticTacToeReducer(state: CellState[][] = initial, action?: Action) {
  if (!action) {
    return state;
  }
  switch (action.type) {
    case ActionType.Restart:
      return initial;
    case ActionType.Shoot:
      return [[CellState.X, CellState.Empty, CellState.Empty]].concat(initial.slice(1));
    default:
      return state;
  }
}

@Injectable()
export class TicTacToeService {
  private actions: Subject<Action> = new Subject();
  readonly state: Observable<CellState[][]>;

  constructor() {
    this.state = this.actions
      .asObservable()
      .scan(ticTacToeReducer, initial)
      .startWith(initial);

    this.actions.subscribe(v => console.log(v));
  }

  restart() {
    this.actions.next({ type: ActionType.Restart });
  }

  shoot(player: CellState, coords: { x: number, y: number }) {
    this.actions.next({ type: ActionType.Shoot, payload: coords });
  }
}
