import { Component, Input, Output, EventEmitter } from '@angular/core';
import { CellState } from '../player.enum';

@Component({
  selector: 'app-board-cell',
  templateUrl: './board-cell.component.html',
  styleUrls: ['./board-cell.component.css']
})
export class BoardCellComponent {
  @Input() value: CellState;
  @Output() shoot = new EventEmitter<undefined>();

  constructor() { }
}
