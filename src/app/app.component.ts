import { Component } from '@angular/core';
import { TicTacToeService } from './tic-tac-toe.service';
import { Observable } from 'rxjs/Observable';
import { CellState } from './player.enum';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';
  board: Observable<CellState[][]>;

  constructor(private ticTacToeService: TicTacToeService) {
    this.board = this.ticTacToeService.state;
  }

  onClick(payload: { x: number, y: number }) {
    this.ticTacToeService.shoot(CellState.X, payload);
  }
}
