import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { CellState } from '../player.enum';

@Component({
  selector: 'app-board',
  templateUrl: './board.component.html',
  styleUrls: ['./board.component.css']
})
export class BoardComponent {
  @Input() board: CellState[][];
  @Output() shoot = new EventEmitter<{ x: number, y: number }>();

  constructor() { }
}
